class CreateLivingSpaces < ActiveRecord::Migration
  def change
    create_table :living_spaces do |t|
      t.float :latitude
      t.float :longitude
      t.integer :type
      t.timestamps
    end
  end
  def down
    drop_table :living_spaces
  end
end
