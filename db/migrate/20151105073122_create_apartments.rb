class CreateApartments < ActiveRecord::Migration
  def change
    create_table :apartments do |t|
      t.string :location
      t.string :longitude
      t.timestamps
    end
  end
  def down
    drop_table :apartments
  end
end
