class AddTitleToComplaints < ActiveRecord::Migration
  def change
    add_column :complaints, :title, :string, :null => false
  end

  def down
    remove_column :complaints, :title
  end
end

