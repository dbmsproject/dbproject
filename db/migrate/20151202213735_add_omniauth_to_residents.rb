class AddOmniauthToResidents < ActiveRecord::Migration
  def change
    add_column :residents, :provider, :string
    add_index :residents, :provider
    add_column :residents, :uid, :string
    add_index :residents, :uid
  end
end
