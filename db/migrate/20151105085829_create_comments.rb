class CreateComments < ActiveRecord::Migration
  def change
    create_table :comments do |t|
			t.string :comment
			t.belongs_to :resident
			t.timestamps
		end
	end
	def down
		drop_table :comments
	end
end
