class AddForeignKey < ActiveRecord::Migration
  def change
    add_reference :workers, :person, index: true, foreign_key: true
    add_reference :posts, :person, index: true, foreign_key: true
    add_reference :residents, :person, index: true, foreign_key: true
  end
end
