class CreateComplaints < ActiveRecord::Migration
  def change
    create_table :complaints do |t|
      t.boolean :solved
      t.string :content
      t.timestamps
    end
  end
  def down
    drop_table :complaints
  end
end