class CreateWorkers < ActiveRecord::Migration
  def change
    create_table :workers do |t|
      t.string :first_name
      t.string :last_name
      t.date :dob
      t.string :gender
      t.timestamps
      t.float :rating
      t.string :address

    end
  end
  def down
    drop_table :workers
  end
end
