class ComplaintBelongsToResident < ActiveRecord::Migration
  def change
    add_belongs_to :complaints, :resident
  end
end
