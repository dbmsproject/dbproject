class AddSuperAdminColumnToResident < ActiveRecord::Migration
  def change
    add_column :residents, :superadmin, :integer, :limit => 1
    # Adding a flag to identify a particular user as superadmin
  end
end
