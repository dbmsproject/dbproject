class CreateResidentialBuildings < ActiveRecord::Migration
  def change
    create_table :residential_buildings do |t|
    t.float :dues
    t.float :latitude
    t.float :longitude
    t.timestamps null: false
    end
  end
	def down
		drop_table :residential_buildings
	end
end
