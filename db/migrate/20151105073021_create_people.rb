class CreatePeople < ActiveRecord::Migration
  def change
    create_table :people do |t|
      t.string :first_name
      t.string :last_name
      t.date :dob
      t.string :gender
      t.timestamps
    end
  end
  def down
    drop_table :people
  end
end
