class CreatePolls < ActiveRecord::Migration
  def change
    create_table :polls do |t|
	t.string :options
	t.boolean :only_admins_can_vote
	t.string :result
	t.timestamps
    end
  end
  def down
	drop_table :polls
  end
end
