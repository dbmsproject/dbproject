class CreateInformation < ActiveRecord::Migration
  def change
    create_table :information do |t|
      t.date :created_on
      t.string :content
      t.integer :sentiment
    end
  end

	def down
		drop_table :information
	end
end
