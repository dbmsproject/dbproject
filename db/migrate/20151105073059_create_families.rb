 class CreateFamilies < ActiveRecord::Migration
  def change
    create_table :families do |t|
      t.float :dues
      t.string :address
      t.string :string
      t.timestamps null: false
    end
  end
  def down
    drop_table :families
  end
end
