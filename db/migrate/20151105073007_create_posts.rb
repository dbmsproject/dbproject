class CreatePosts < ActiveRecord::Migration
  def change
    create_table :posts do |t|
      t.string :expiry_date
      t.text :text , null:false
      t.timestamps null: false
      t.belongs_to :resident
    end
  end
    def down
      drop_table :posts
    end
end
