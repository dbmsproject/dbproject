class CreateFlats < ActiveRecord::Migration
  def change
    create_table :flats do |t|
	t.integer :floor
	t.integer :flat_type
	t.float :dues
  t.belongs_to :apartment
	t.timestamps null: false
    end
  end
  def down
	drop_table :flats
  end
end
