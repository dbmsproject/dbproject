class AddColumnApprovedToResident < ActiveRecord::Migration
  def change
    add_column :residents, :approved, :integer, :limit => 1, :default=>0
  end
end
