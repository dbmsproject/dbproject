class ModifyPostTable < ActiveRecord::Migration
  def change
    rename_column :posts, :text, :content
    change_column :posts, :content, :text, :null => false
    add_column :posts, :title, :string, length: 100
  end

  def down
    rename_column :posts, :content, :text
    remove_column :posts, :title
  end
end