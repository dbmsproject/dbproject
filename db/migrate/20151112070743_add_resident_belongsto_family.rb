class AddResidentBelongstoFamily < ActiveRecord::Migration
  def change
    change_table :residents do |t|
      t.belongs_to :family
    end
  end
end
