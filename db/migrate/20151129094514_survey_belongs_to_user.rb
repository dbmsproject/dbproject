class SurveyBelongsToUser < ActiveRecord::Migration
  def change
    add_belongs_to :survey_surveys, :resident
  end
end
