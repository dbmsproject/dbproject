class CreateResidents < ActiveRecord::Migration
  def change
    create_table :residents do |t|
      t.string :first_name
      t.string :last_name
      t.date :dob
      t.string :email
      t.string :gender
      t.timestamps
      t.belongs_to :living_space
    end
  end
  def down
   drop_table :residents
  end
end
