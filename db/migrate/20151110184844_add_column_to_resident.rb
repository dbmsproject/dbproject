class AddColumnToResident < ActiveRecord::Migration
  def change
    add_column :residents, :isadmin, :integer, default: 0
  end

  def down
    remove_column :residents, :isadmin
  end
end
