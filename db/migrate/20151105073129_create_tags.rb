class CreateTags < ActiveRecord::Migration
  def change
    create_table :tags do |t|
			t.string :name
      t.integer :use_count
    end
  end

	def down
		drop_table :tags
	end
end
