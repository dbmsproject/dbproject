class RemoveStringFromFamilies < ActiveRecord::Migration
  def change
    remove_column :families, :string
    remove_column :families, :address
    add_column :families, :head, :integer, :null => false
    add_belongs_to :families, :living_space
    # head will contain the id of the head of the family
  end
end
