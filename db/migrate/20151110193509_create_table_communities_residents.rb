class CreateTableCommunitiesResidents < ActiveRecord::Migration
  def change
    create_table :communities_residents do |t|
      t.belongs_to :community, index:true
      t.belongs_to :resident, index:true
    end
  end
end
