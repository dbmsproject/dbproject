# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20151202213735) do

  create_table "apartments", force: :cascade do |t|
    t.string   "location",   limit: 255
    t.string   "longitude",  limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "comments", force: :cascade do |t|
    t.string   "body",        limit: 255
    t.integer  "resident_id", limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "post_id",     limit: 4
  end

  create_table "communities", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "communities_residents", force: :cascade do |t|
    t.integer "community_id", limit: 4
    t.integer "resident_id",  limit: 4
  end

  add_index "communities_residents", ["community_id"], name: "index_communities_residents_on_community_id", using: :btree
  add_index "communities_residents", ["resident_id"], name: "index_communities_residents_on_resident_id", using: :btree

  create_table "complaints", force: :cascade do |t|
    t.boolean  "solved",     limit: 1
    t.string   "content",    limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "families", force: :cascade do |t|
    t.float    "dues",            limit: 24
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
    t.integer  "head",            limit: 4,  null: false
    t.integer  "living_space_id", limit: 4
  end

  create_table "flats", force: :cascade do |t|
    t.integer  "floor",        limit: 4
    t.integer  "flat_type",    limit: 4
    t.float    "dues",         limit: 24
    t.integer  "Apartment_id", limit: 4
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
  end

  create_table "information", force: :cascade do |t|
    t.date    "created_on"
    t.string  "content",    limit: 255
    t.integer "sentiment",  limit: 4
  end

  create_table "living_spaces", force: :cascade do |t|
    t.float    "latitude",   limit: 24
    t.float    "longitude",  limit: 24
    t.integer  "type",       limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "neighbourhoods", force: :cascade do |t|
  end

  create_table "people", force: :cascade do |t|
    t.string   "first_name", limit: 255
    t.string   "last_name",  limit: 255
    t.date     "dob"
    t.string   "gender",     limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "polls", force: :cascade do |t|
    t.string   "options",              limit: 255
    t.boolean  "only_admins_can_vote", limit: 1
    t.string   "result",               limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "posts", force: :cascade do |t|
    t.text     "content",     limit: 65535, null: false
    t.string   "expiry_date", limit: 255
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
    t.integer  "resident_id", limit: 4
    t.integer  "person_id",   limit: 4
    t.string   "title",       limit: 255
  end

  add_index "posts", ["person_id"], name: "index_posts_on_person_id", using: :btree

  create_table "residential_buildings", force: :cascade do |t|
    t.float    "dues",       limit: 24
    t.float    "latitude",   limit: 24
    t.float    "longitude",  limit: 24
    t.datetime "created_at",            null: false
    t.datetime "updated_at",            null: false
  end

  create_table "residents", force: :cascade do |t|
    t.string   "first_name",             limit: 255
    t.string   "last_name",              limit: 255
    t.date     "dob"
    t.string   "email",                  limit: 255
    t.string   "gender",                 limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "living_space_id",        limit: 4
    t.integer  "person_id",              limit: 4
    t.string   "encrypted_password",     limit: 255, default: "", null: false
    t.string   "reset_password_token",   limit: 255
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          limit: 4,   default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip",     limit: 255
    t.string   "last_sign_in_ip",        limit: 255
    t.integer  "isadmin",                limit: 4,   default: 0
    t.integer  "superadmin",             limit: 1
    t.integer  "family_id",              limit: 4
    t.integer  "approved",               limit: 1,   default: 0
    t.string   "provider",               limit: 255
    t.string   "uid",                    limit: 255
  end

  add_index "residents", ["email"], name: "index_residents_on_email", unique: true, using: :btree
  add_index "residents", ["person_id"], name: "index_residents_on_person_id", using: :btree
  add_index "residents", ["provider"], name: "index_residents_on_provider", using: :btree
  add_index "residents", ["reset_password_token"], name: "index_residents_on_reset_password_token", unique: true, using: :btree
  add_index "residents", ["uid"], name: "index_residents_on_uid", using: :btree

  create_table "survey_answers", force: :cascade do |t|
    t.integer  "attempt_id",  limit: 4
    t.integer  "question_id", limit: 4
    t.integer  "option_id",   limit: 4
    t.boolean  "correct",     limit: 1
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "survey_attempts", force: :cascade do |t|
    t.integer "participant_id",   limit: 4
    t.string  "participant_type", limit: 255
    t.integer "survey_id",        limit: 4
    t.boolean "winner",           limit: 1
    t.integer "score",            limit: 4
  end

  create_table "survey_options", force: :cascade do |t|
    t.integer  "question_id", limit: 4
    t.integer  "weight",      limit: 4,   default: 0
    t.string   "text",        limit: 255
    t.boolean  "correct",     limit: 1
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "survey_questions", force: :cascade do |t|
    t.integer  "survey_id",  limit: 4
    t.string   "text",       limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "survey_surveys", force: :cascade do |t|
    t.string   "name",            limit: 255
    t.text     "description",     limit: 65535
    t.integer  "attempts_number", limit: 4,     default: 0
    t.boolean  "finished",        limit: 1,     default: false
    t.boolean  "active",          limit: 1,     default: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "survey_type",     limit: 4
    t.integer  "resident_id",     limit: 4
  end

  create_table "tags", force: :cascade do |t|
    t.string  "name",      limit: 255
    t.integer "use_count", limit: 4
  end

  create_table "votes", force: :cascade do |t|
    t.integer  "votable_id",   limit: 4
    t.string   "votable_type", limit: 255
    t.integer  "voter_id",     limit: 4
    t.string   "voter_type",   limit: 255
    t.boolean  "vote_flag",    limit: 1
    t.string   "vote_scope",   limit: 255
    t.integer  "vote_weight",  limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "votes", ["votable_id", "votable_type", "vote_scope"], name: "index_votes_on_votable_id_and_votable_type_and_vote_scope", using: :btree
  add_index "votes", ["voter_id", "voter_type", "vote_scope"], name: "index_votes_on_voter_id_and_voter_type_and_vote_scope", using: :btree

  create_table "workers", force: :cascade do |t|
    t.string   "first_name", limit: 255
    t.string   "last_name",  limit: 255
    t.date     "dob"
    t.string   "gender",     limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
    t.float    "rating",     limit: 24
    t.string   "address",    limit: 255
    t.integer  "person_id",  limit: 4
  end

  add_index "workers", ["person_id"], name: "index_workers_on_person_id", using: :btree

  add_foreign_key "posts", "people"
  add_foreign_key "residents", "people"
  add_foreign_key "workers", "people"
end
