json.array!(@residential_buildings) do |residential_building|
  json.extract! residential_building, :id, :dues, :latitude, :longitude
  json.url residential_building_url(residential_building, format: :json)
end
