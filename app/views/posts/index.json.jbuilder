json.array!(@posts) do |post|
  json.extract! post, :id, :expiry_date, :resident_id
  json.url post_url(post, format: :json)
end
