json.array!(@information) do |information|
  json.extract! information, :id, :created_on, :content, :sentiment
  json.url information_url(information, format: :json)
end
