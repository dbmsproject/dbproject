json.array!(@members) do |member|
  json.extract! member, :id, :resident_id, :community_id
  json.url member_url(member, format: :json)
end
