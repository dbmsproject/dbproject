json.array!(@families) do |family|
  json.extract! family, :id, :dues, :address, :string
  json.url family_url(family, format: :json)
end
