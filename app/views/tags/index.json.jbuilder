json.array!(@tags) do |tag|
  json.extract! tag, :id, :name, :use_count
  json.url tag_url(tag, format: :json)
end
