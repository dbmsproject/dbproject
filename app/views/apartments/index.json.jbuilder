json.array!(@apartments) do |apartment|
  json.extract! apartment, :id, :location, :longitude
  json.url apartment_url(apartment, format: :json)
end
