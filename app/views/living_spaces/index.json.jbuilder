json.array!(@living_spaces) do |living_space|
  json.extract! living_space, :id, :latitude, :longitude, :type
  json.url living_space_url(living_space, format: :json)
end
