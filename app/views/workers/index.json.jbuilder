json.array!(@workers) do |worker|
  json.extract! worker, :id, :first_name, :last_name, :dob, :gender, :rating, :address
  json.url worker_url(worker, format: :json)
end
