json.array!(@flats) do |flat|
  json.extract! flat, :id, :floor, :flat_type, :dues
  json.url flat_url(flat, format: :json)
end
