json.array!(@residents) do |resident|
  json.extract! resident, :id, :first_name, :last_name, :dob, :email, :gender, :living_space_id
  json.url resident_url(resident, format: :json)
end
