json.array!(@polls) do |poll|
  json.extract! poll, :id, :options, :only_admins_can_vote, :result
  json.url poll_url(poll, format: :json)
end
