class LivingSpace < ActiveRecord::Base
  has_one :family
  belongs_to :neighbourhood
end
