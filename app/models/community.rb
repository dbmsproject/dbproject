class Community < ActiveRecord::Base
  has_and_belongs_to_many :residents
  has_and_belongs_to_many :workers

  has_many :members
end
