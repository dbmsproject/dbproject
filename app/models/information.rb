class Information < ActiveRecord::Base
  has_many :tags
  has_many :comments
  belongs_to :resident
end
