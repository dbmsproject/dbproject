class Resident < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  validates_presence_of :first_name, length: {minimum:2}
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable, :omniauthable,
         :omniauth_providers => [:facebook]
  belongs_to :family
  belongs_to :neighbourhood
  belongs_to :living_space
  has_many :posts
  has_and_belongs_to_many :communities
  has_many :comments
  has_many :polls
  has_many :informations
  has_many :members

  has_surveys

  def self.from_omniauth(auth)
    where(provider: auth.provider, uid: auth.uid).first_or_create do |resident|
      resident.email = auth.info.email
      resident.password = Devise.friendly_token[0,20]
      resident.first_name = auth.info.name   # assuming the user model has a name
      resident.last_name = "."
      print "Image coming here"
      #user.image = auth.info.image # assuming the user model has an image
    end
  end
end
