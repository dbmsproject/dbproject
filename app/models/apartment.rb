class Apartment < ActiveRecord::Base
  belongs_to :neighbourhood
  has_many :flats
end
