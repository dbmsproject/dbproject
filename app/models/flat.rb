class Flat < ActiveRecord::Base
  belongs_to :apartment
  has_many :residents
end
