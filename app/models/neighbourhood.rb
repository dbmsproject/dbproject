class Neighbourhood < ActiveRecord::Base
  has_many :residents
  has_many :living_spaces
  has_many :apartments
end
