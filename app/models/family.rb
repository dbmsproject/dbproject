class Family < ActiveRecord::Base
  has_many :residents
  belongs_to :living_space
end
