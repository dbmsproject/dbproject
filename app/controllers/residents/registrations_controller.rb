class Residents::RegistrationsController < Devise::RegistrationsController
  before_filter :configure_sign_up_params, only: [:create]
  before_filter :configure_account_update_params, only: [:update]

  # GET /resource/sign_up
  # def new
  #   super
  # end

  # POST /resource
  # def create
  #   @resident = Resident.new(resident_params)
  #
  #   respond_to do |format|
  #     if @resident.save
  #       format.html { redirect_to @resident, notice: 'Resident was successfully created.' }
  #       format.json { render :show, status: :created, location: @recipe }
  #     else
  #       format.html { render :new }
  #       format.json { render json: @recipe.errors, status: :unprocessable_entity }
  #     end
  #   end
  # end

  # GET /resource/edit
  # def edit
  #   super
  # end

  # PUT /resource
  # def update
  #   super
  # end

  # DELETE /resource
  # def destroy
  #   super
  # end

  # GET /resource/cancel
  # Forces the session data which is usually expired after sign
  # in to be expired now. This is useful if the user wants to
  # cancel oauth signing in/up in the middle of the process,
  # removing all OAuth session data.
  # def cancel
  #   super
  # end

  # protected

  # If you have extra params to permit, append them to the sanitizer.
  def configure_sign_up_params
    devise_parameter_sanitizer.for(:sign_up) << :first_name << :last_name << :living_space_id << :family_id
  end

  # If you have extra params to permit, append them to the sanitizer.
  def configure_account_update_params
    #devise_parameter_sanitizer.for(:account_update) << :first_name << :last_name << :living_space_id
    params.require(:resident).permit(:first_name, :last_name)
  end

  # The path used after sign up.
  def after_sign_up_path_for(resource)
    '/residents/me'
  end

  def after_sign_in_path_for(resource)
    '/residents/me'
  end
  # The path used after sign up for inactive accounts.
  # def after_inactive_sign_up_path_for(resource)
  #   super(resource)
  # end
end
