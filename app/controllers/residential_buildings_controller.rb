class ResidentialBuildingsController < ApplicationController
  before_action :set_residential_building, only: [:show, :edit, :update, :destroy]

  # GET /residential_buildings
  # GET /residential_buildings.json
  def index
    @residential_buildings = ResidentialBuilding.all
  end

  # GET /residential_buildings/1
  # GET /residential_buildings/1.json
  def show
  end

  # GET /residential_buildings/new
  def new
    @residential_building = ResidentialBuilding.new
  end

  # GET /residential_buildings/1/edit
  def edit
  end

  # POST /residential_buildings
  # POST /residential_buildings.json
  def create
    @residential_building = ResidentialBuilding.new(residential_building_params)

    respond_to do |format|
      if @residential_building.save
        format.html { redirect_to @residential_building, notice: 'Residential building was successfully created.' }
        format.json { render :show, status: :created, location: @residential_building }
      else
        format.html { render :new }
        format.json { render json: @residential_building.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /residential_buildings/1
  # PATCH/PUT /residential_buildings/1.json
  def update
    respond_to do |format|
      if @residential_building.update(residential_building_params)
        format.html { redirect_to @residential_building, notice: 'Residential building was successfully updated.' }
        format.json { render :show, status: :ok, location: @residential_building }
      else
        format.html { render :edit }
        format.json { render json: @residential_building.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /residential_buildings/1
  # DELETE /residential_buildings/1.json
  def destroy
    @residential_building.destroy
    respond_to do |format|
      format.html { redirect_to residential_buildings_url, notice: 'Residential building was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_residential_building
      @residential_building = ResidentialBuilding.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def residential_building_params
      params.require(:residential_building).permit(:dues, :latitude, :longitude)
    end
end
