class ResidentsController < ApplicationController
  before_action :set_resident, only: [:show, :edit, :update, :destroy, :me, :approve]
  before_action :authenticate_resident!, only: [:edit, :update, :destroy, :set_resident, :me]


  # GET /residents
  # GET /residents.json
  def index
    @residents = Resident.all
  end


  # GET /residents/1
  # GET /residents/1.json
  def show
    begin
      @resident = Resident.find(params[:id])
    rescue
      @resident = nil
    end
  end

  def me
    begin
      @resident = Resident.find(current_resident.id)
      @post = @resident.posts
    rescue
      @resident = nil
    end
  end

  # GET /residents/new
  def new
    render :text => "Please use /register/sign_up"
    #@resident = Resident.new
  end

  # GET /residents/1/edit
  def edit
    id = current_resident.id
    if id!=params[:id]
      @message = "You are allowed to edit only your account"
    end
    @resident = Resident.find(id)
  end

  # POST /residents
  # POST /residents.json
  def create
    @resident = Resident.new(resident_params)

    respond_to do |format|
      if @resident.save
        format.html { redirect_to @resident, notice: 'Resident was successfully created.' }
        format.json { render :show, status: :created, location: @resident }
      else
        format.html { render :new }
        format.json { render json: @resident.errors, status: :unprocessable_entity }
      end
    end
  end

  def logout
    sign_out
    @resident
  end

  # PATCH/PUT /residents/1
  # PATCH/PUT /residents/1.json
  def update
    respond_to do |format|
      if @resident.update(resident_params)
        format.html { redirect_to '/residents/me', notice: 'Resident was successfully updated.' }
        format.json { render :me, status: :ok, location: @resident }
      else
        format.html { render :edit }
        format.json { render json: @resident.errors, status: :unprocessable_entity }
      end
    end
  end

  def approve
    # Used for approving an account by Admin
    # Bug here : A user who creates a family is able to approve himself. Need to fix it before demo
    temp = Resident.find(params[:id])
    if(@resident.superadmin==0 and @resident.id!=temp.family.head)
      redirect_to '/residents/me', notice: 'You dont have privileges'
    else
      if(@resident.superadmin==0 and @resident.id==temp.id)
        redirect_to '/residents/me', notice: 'You cannot approve your account. Ask superadmin to approve your account'
      else
        temp.approved=1
        temp.save
        redirect_to '/residents/me', notice: params[:email]+' Approved.'
      end
    end

  end
  # DELETE /residents/1
  # DELETE /residents/1.json
  def destroy
    @resident.destroy
    respond_to do |format|
      format.html { redirect_to residents_url, notice: 'Resident was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_resident
    @resident = current_resident
    # this ensures that the person logged in can edit and delete his own account
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def resident_params
    params.require(:resident).permit(:first_name, :last_name, :dob, :email, :gender, :living_space_id, :family_id)
  end
end
