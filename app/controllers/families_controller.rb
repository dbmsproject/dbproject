class FamiliesController < ApplicationController
  before_action :set_family, only: [:show, :edit, :update, :destroy]

  # GET /families
  # GET /families.json
  def index
    @families = Family.all
  end

  # GET /families/1
  # GET /families/1.json
  def show
  end

  # GET /families/new
  def new
    @family = Family.new
  end

  # GET /families/1/edit
  def edit
  end

  # POST /families
  # POST /families.json
  def create
    # A small flaw. One member can create more than 1 family right now. Will fix that later. Good enough for demo this one.
    @family = Family.new(family_params)
    @family.head = current_resident.id

    if @family.save
      resident = Resident.find(current_resident.id)
      resident.family_id = @family.id
      resident.save

      redirect_to  "/residents/me", notice: "Family with ID: "+@family.id.to_s+" Created successfully. You are head of the family."
    else
      render :new
      render json: @family.errors, status: :unprocessable_entity
    end


  end

  # PATCH/PUT /families/1
  # PATCH/PUT /families/1.json
  def update
    respond_to do |format|
      if @family.update(family_params)
        format.html { redirect_to @family, notice: 'Family was successfully updated.' }
        format.json { render :show, status: :ok, location: @family }
      else
        format.html { render :edit }
        format.json { render json: @family.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /families/1
  # DELETE /families/1.json
  def destroy
    @family.destroy
    respond_to do |format|
      format.html { redirect_to families_url, notice: 'Family was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_family
    @family = Family.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def family_params
    #params.require(:family).permit(:dues, :address, :string)
  end
end
