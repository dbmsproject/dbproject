class LivingSpacesController < ApplicationController
  before_action :set_living_space, only: [:show, :edit, :update, :destroy]

  # GET /living_spaces
  # GET /living_spaces.json
  def index
    @living_spaces = LivingSpace.all
  end

  # GET /living_spaces/1
  # GET /living_spaces/1.json
  def show
  end

  # GET /living_spaces/new
  def new
    @living_space = LivingSpace.new
  end

  # GET /living_spaces/1/edit
  def edit
  end

  # POST /living_spaces
  # POST /living_spaces.json
  def create
    @living_space = LivingSpace.new(living_space_params)

    respond_to do |format|
      if @living_space.save
        format.html { redirect_to @living_space, notice: 'Living space was successfully created.' }
        format.json { render :show, status: :created, location: @living_space }
      else
        format.html { render :new }
        format.json { render json: @living_space.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /living_spaces/1
  # PATCH/PUT /living_spaces/1.json
  def update
    respond_to do |format|
      if @living_space.update(living_space_params)
        format.html { redirect_to @living_space, notice: 'Living space was successfully updated.' }
        format.json { render :show, status: :ok, location: @living_space }
      else
        format.html { render :edit }
        format.json { render json: @living_space.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /living_spaces/1
  # DELETE /living_spaces/1.json
  def destroy
    @living_space.destroy
    respond_to do |format|
      format.html { redirect_to living_spaces_url, notice: 'Living space was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_living_space
      @living_space = LivingSpace.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def living_space_params
      params.require(:living_space).permit(:latitude, :longitude, :type)
    end
end
