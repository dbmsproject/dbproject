require 'test_helper'

class LivingSpacesControllerTest < ActionController::TestCase
  setup do
    @living_space = living_spaces(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:living_spaces)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create living_space" do
    assert_difference('LivingSpace.count') do
      post :create, living_space: { latitude: @living_space.latitude, longitude: @living_space.longitude, type: @living_space.type }
    end

    assert_redirected_to living_space_path(assigns(:living_space))
  end

  test "should show living_space" do
    get :show, id: @living_space
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @living_space
    assert_response :success
  end

  test "should update living_space" do
    patch :update, id: @living_space, living_space: { latitude: @living_space.latitude, longitude: @living_space.longitude, type: @living_space.type }
    assert_redirected_to living_space_path(assigns(:living_space))
  end

  test "should destroy living_space" do
    assert_difference('LivingSpace.count', -1) do
      delete :destroy, id: @living_space
    end

    assert_redirected_to living_spaces_path
  end
end
