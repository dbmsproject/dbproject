require 'test_helper'

class ResidentialBuildingsControllerTest < ActionController::TestCase
  setup do
    @residential_building = residential_buildings(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:residential_buildings)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create residential_building" do
    assert_difference('ResidentialBuilding.count') do
      post :create, residential_building: { dues: @residential_building.dues, latitude: @residential_building.latitude, longitude: @residential_building.longitude }
    end

    assert_redirected_to residential_building_path(assigns(:residential_building))
  end

  test "should show residential_building" do
    get :show, id: @residential_building
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @residential_building
    assert_response :success
  end

  test "should update residential_building" do
    patch :update, id: @residential_building, residential_building: { dues: @residential_building.dues, latitude: @residential_building.latitude, longitude: @residential_building.longitude }
    assert_redirected_to residential_building_path(assigns(:residential_building))
  end

  test "should destroy residential_building" do
    assert_difference('ResidentialBuilding.count', -1) do
      delete :destroy, id: @residential_building
    end

    assert_redirected_to residential_buildings_path
  end
end
